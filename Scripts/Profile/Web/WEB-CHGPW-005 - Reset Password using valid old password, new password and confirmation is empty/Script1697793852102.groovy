import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://demo-app.site/')

WebUI.click(findTestObject('Object Repository/Login/a_Masuk'))

WebUI.setText(findTestObject('Login/input_email'), 'sdivo164@gmail.com')

WebUI.setText(findTestObject('Login/set-text_password-login'), 'belajar123')

WebUI.click(findTestObject('Login/button_Login'))

WebUI.click(findTestObject('Object Repository/Profile/Change Password/logo_profile'))

WebUI.click(findTestObject('Object Repository/Profile/Change Password/my_account'))

WebUI.click(findTestObject('Object Repository/Profile/Change Password/span_Profil'))

WebUI.click(findTestObject('Object Repository/Profile/Change Password/field_Change Password'))

WebUI.setText(findTestObject('Object Repository/Profile/Change Password/settext_Old Password'), 'belajar123')

WebUI.setText(findTestObject('Profile/Change Password/set-text_New Password - Profile'), '')

WebUI.setText(findTestObject('Profile/Change Password/set-text_Confirm Password'), '')

WebUI.click(findTestObject('Object Repository/Profile/Change Password/button_Save Changes'))

WebUI.verifyElementVisible(findTestObject('Profile/Change Password/button_Save Changes'))

WebUI.closeBrowser()

