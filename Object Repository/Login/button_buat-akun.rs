<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_buat-akun</name>
   <tag></tag>
   <elementGuidId>e8bd2229-486f-43a3-b079-fc419d1d77a3</elementGuidId>
   <imagePath>Screenshots/Targets/Page_Be a Profressional Talent with Coding.ID/button_buat-akun.png</imagePath>
   <selectorCollection>
      <entry>
         <key>IMAGE</key>
         <value>Screenshots/Targets/Page_Be a Profressional Talent with Coding.ID/button_buat-akun.png</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>button.buttonBanner.btn-sm</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='navbar-collapse-1']/ul/li[8]/form/button</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>28d91091-a5c7-4b14-8d64-72b13cb3f1d5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>buttonBanner btn-sm</value>
      <webElementGuid>a3a9a9d8-a659-4087-be0e-406c7d06e6a7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Buat
                                                        Akun</value>
      <webElementGuid>d1a00a3b-54da-4f22-aa2c-ad4152a4433c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;navbar-collapse-1&quot;)/ul[@class=&quot;nav navbar-nav&quot;]/li[@class=&quot;wm-megamenu-li&quot;]/form[1]/button[@class=&quot;buttonBanner btn-sm&quot;]</value>
      <webElementGuid>31718fa4-83ff-45db-838c-d017546c56b0</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='navbar-collapse-1']/ul/li[8]/form/button</value>
      <webElementGuid>f04f48e4-5e90-4b08-aeef-ac6f321bed0d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Masuk'])[1]/following::button[1]</value>
      <webElementGuid>134f18f1-1d91-4aa3-90fe-fe333d50d3be</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Kontak'])[1]/following::button[1]</value>
      <webElementGuid>90c3ac92-5b7f-48ac-852e-4b94c8e08253</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Tutup'])[1]/preceding::button[1]</value>
      <webElementGuid>d137ecc1-a2d7-4a36-967c-db935e8aa2fd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Course'])[2]/preceding::button[2]</value>
      <webElementGuid>92c6b6f9-f63a-42b6-9437-309a363911fb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[8]/form/button</value>
      <webElementGuid>353002c6-edb6-49a0-be07-c2ad63bfc269</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[(text() = 'Buat
                                                        Akun' or . = 'Buat
                                                        Akun')]</value>
      <webElementGuid>ab487692-2395-4469-b39b-c5c8fd36b540</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
