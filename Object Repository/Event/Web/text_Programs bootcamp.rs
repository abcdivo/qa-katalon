<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>text_Programs bootcamp</name>
   <tag></tag>
   <elementGuidId>10304d15-02db-4938-891a-8296b7bfa478</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>h2.titleEvent</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='containerEvent']/div/div/div/h2</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h2</value>
      <webElementGuid>88a2fdff-3fec-4eea-80ba-8444ac5c3946</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>titleEvent</value>
      <webElementGuid>814d4039-669c-4c03-95b6-98b60e6c6137</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Programs</value>
      <webElementGuid>401fe14a-7fb5-4ae9-9eb0-3ad5e3d73c1d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;containerEvent&quot;)/div[1]/div[@class=&quot;row containerDrop&quot;]/div[@class=&quot;col-md-8&quot;]/h2[@class=&quot;titleEvent&quot;]</value>
      <webElementGuid>0a256484-6937-4023-a253-e45f5eece9fc</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='containerEvent']/div/div/div/h2</value>
      <webElementGuid>110bc8e4-f574-4e27-85e7-e7fba54a90b3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='CODING.ID Programs'])[1]/following::h2[1]</value>
      <webElementGuid>30676da3-f5a4-4113-b5be-65a41e658eac</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Quality Assurance Engineer Class'])[2]/preceding::h2[1]</value>
      <webElementGuid>20e67820-5614-4957-b38d-939ae18241af</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Jadi QA Engineer hanya dalam 2 bulan dengan Jaminan Kerja dan Bea...'])[1]/preceding::h2[1]</value>
      <webElementGuid>d66084d9-8548-4f49-a599-a800cf65a392</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Programs']/parent::*</value>
      <webElementGuid>06a946bd-e2f9-4cf0-b019-cb94baa1e3e5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/div/h2</value>
      <webElementGuid>97d08afe-782e-40a9-9520-1145e76f2d3b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h2[(text() = 'Programs' or . = 'Programs')]</value>
      <webElementGuid>d05234be-017d-4a3d-9cab-f32f0fb8ef75</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
