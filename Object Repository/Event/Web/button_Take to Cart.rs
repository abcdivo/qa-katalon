<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Take to Cart</name>
   <tag></tag>
   <elementGuidId>c568f4f4-f0ff-45ab-a8f0-5f20f4bbf278</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>button.btn.btn-light.buttonLoad</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='jumbotronCourseDetail']/div/a/button</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>a3436be5-beed-4edd-b7fb-57a7ac0e0e06</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-light buttonLoad</value>
      <webElementGuid>369e8d4a-bbfa-4c52-bd69-57ba9f5cfe82</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>x-bind:class</name>
      <type>Main</type>
      <value>modalOpen ? 'modal-open' : ''</value>
      <webElementGuid>a490eef6-cb5a-4d5f-8a1b-ce88ce03e9e6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Take
                            to Cart
                        </value>
      <webElementGuid>3634c184-7ee6-4b2e-91b4-a43f48467567</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;jumbotronCourseDetail&quot;)/div[@class=&quot;textJumbotron&quot;]/a[1]/button[@class=&quot;btn btn-light buttonLoad&quot;]</value>
      <webElementGuid>0771a201-ea2e-4ffb-9124-e968e15bc7b3</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='jumbotronCourseDetail']/div/a/button</value>
      <webElementGuid>6a537c20-796d-4d24-aad8-ec79c705a8f9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Basic Programming with Javascript'])[1]/following::button[1]</value>
      <webElementGuid>059f2126-68aa-4ce9-b2f2-3ea34e1e7d37</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Back to Course >'])[1]/preceding::button[1]</value>
      <webElementGuid>fcbe40c3-e015-49d1-8a08-5c4bc4e80952</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Basic Programming with Javascript'])[2]/preceding::button[1]</value>
      <webElementGuid>d766c965-e92e-4ed6-a81b-ea7e30aa65c3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//a/button</value>
      <webElementGuid>efc9fd9d-30af-45c0-bb93-a6675d517c81</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[(text() = 'Take
                            to Cart
                        ' or . = 'Take
                            to Cart
                        ')]</value>
      <webElementGuid>c4773064-ab37-4da6-81af-768a0fdc3955</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
