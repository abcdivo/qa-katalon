<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>text_Sorry we cant find any course, may be you have not enroll yet</name>
   <tag></tag>
   <elementGuidId>b3d81ef8-d422-42da-ad59-3224268477fe</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>p.lead</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='app']/div/div[3]/section/div/div/div/div/div[2]/div/div[2]/p</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>p</value>
      <webElementGuid>57d8f919-607f-4255-97ea-5fd7729276be</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>lead</value>
      <webElementGuid>e12cd2f8-a73d-4607-b0fc-278bda60f1ba</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                Sorry we can't find any course, may be you have not enroll yet.
            </value>
      <webElementGuid>289d9a59-37ff-4505-846f-7abe24ed05a5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[@class=&quot;main-wrapper main-wrapper-1&quot;]/div[@class=&quot;main-content&quot;]/section[@class=&quot;section&quot;]/div[@class=&quot;section-body&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;card&quot;]/div[@class=&quot;card-body&quot;]/div[1]/div[@class=&quot;empty-state&quot;]/p[@class=&quot;lead&quot;]</value>
      <webElementGuid>954b8c08-d5bd-4407-87b2-93eb165727be</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/div/div[3]/section/div/div/div/div/div[2]/div/div[2]/p</value>
      <webElementGuid>f600ebea-82d2-47cd-a67e-fdf1904af28e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)=concat('We couldn', &quot;'&quot;, 't find any course')])[1]/following::p[1]</value>
      <webElementGuid>d2880f6d-1a1a-46b7-a929-eea33c35a7b3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Page'])[1]/following::p[1]</value>
      <webElementGuid>04df35ae-dc5c-4be6-832f-be85c0e532fe</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='See all Course'])[1]/preceding::p[1]</value>
      <webElementGuid>e4f0f830-6783-4022-8fbe-593d181949ed</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Delete'])[1]/preceding::p[1]</value>
      <webElementGuid>a4a353dc-38b7-4693-9cfd-b79f9be9248e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//p</value>
      <webElementGuid>f7d14fb6-eea3-4b8b-9227-97f8eec2fd7e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//p[(text() = concat(&quot;
                Sorry we can&quot; , &quot;'&quot; , &quot;t find any course, may be you have not enroll yet.
            &quot;) or . = concat(&quot;
                Sorry we can&quot; , &quot;'&quot; , &quot;t find any course, may be you have not enroll yet.
            &quot;))]</value>
      <webElementGuid>226b49f2-4a0a-4d16-9bf5-c9011a9c8d0d</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
